
package sqlconnection;
import java.awt.Button;
import java.awt.Panel;
import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
public class SqlConnection {

    public static void main(String[] args) {
        String driver="com.mysql.cj.jdbc.Driver";
        Connection conexión=null;
        Panel pan=new Panel();
        Button but=new Button();
        but.setName("butonTest");
        but.setLocation(10, 10);
        but.setSize(60,20);
        pan.setName("panelTest");
        pan.setSize(120,120);
        try{
            Class.forName(driver);
            conexión= DriverManager.getConnection("jdbc:mysql://localhost/universidad?serverTimezone=UTC","root","root");
            JOptionPane.showMessageDialog(null,"Conexión exitosa");
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
            System.out.println(e.getMessage());
        }
        try{
        Statement anyCode=conexión.createStatement();
        PreparedStatement insertValue=conexión.prepareStatement("Insert into calificaciones (nota) values (?),(?),(?)");
        insertValue.setInt(1,4);
        insertValue.setInt(2,3);
        insertValue.setInt(3,2);
        insertValue.executeUpdate();
        anyCode.execute("create database root");
        conexión=DriverManager.getConnection("jdbc:mysql://localhost/afinity?serverTimezone=UTC","root","root");
        String[] root={"triatoma","triatomaID","levelSkill"};
        Statement createTable=conexión.createStatement();
        createTable.execute(statementTable(root));
        root[1]="invegia";
        root[2]="invegiaId";
        createTable.execute(statementTable(root));
        }
        catch(SQLException i){
            JOptionPane.showMessageDialog(null,i.getMessage());
        }
    }
     static String statementTable(String[] root){
        return "create table "+root[0]+" ("+root[1]+" nvarchar[20], "+root[3]+" int)";
    }
}
